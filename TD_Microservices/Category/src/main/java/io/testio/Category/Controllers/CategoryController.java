package io.testio.Category.Controllers;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import io.testio.Category.Services.CategoryService;
import io.testio.Category.models.CategoryEntity;

@RestController
public class CategoryController {

    @Autowired
    private CategoryService service;

    @GetMapping(value = "/category")
    public List<CategoryEntity> getAllCategories() {
        return service.getAllCategories();
    }

    @GetMapping(value = "/category/{id}")
    public CategoryEntity getCEntity(@PathVariable String id) {
        return service.getCategory(id);
    }

    @PostMapping(value = "/category")
    public void addCategory(@RequestBody CategoryEntity category) {
        service.saveCategory(category);

    }

    @DeleteMapping(value = "/category/{id}")
    public void deleteCategory(@PathVariable Long id) {
        service.deleteCategory(id);
    }

}