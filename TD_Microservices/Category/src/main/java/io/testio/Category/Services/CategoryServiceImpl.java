package io.testio.Category.Services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import io.testio.Category.Repository.CategoryRepository;
import io.testio.Category.models.CategoryEntity;

@Service
public class CategoryServiceImpl implements CategoryService {

    @Autowired
    private CategoryRepository repository;

    @Override
    public List<CategoryEntity> getAllCategories() {
        List<CategoryEntity> cat = (List<CategoryEntity>) repository.findAll();
        return cat;
    }

    @Override
    public CategoryEntity getCategory(String name) {
        Optional<CategoryEntity> optCat = repository.findDistinctByName(name);
        return optCat.get();
    }

    @Override
    public void saveCategory(CategoryEntity category) {
        repository.save(category);
    }

    @Override
    public void deleteCategory(Long id) {
        repository.deleteById(id);
    }
}
