package io.testio.Category.Services;

import java.util.List;
import io.testio.Category.models.CategoryEntity;

public interface CategoryService {

    public List<CategoryEntity> getAllCategories();

    public CategoryEntity getCategory(String name);

    public void saveCategory(CategoryEntity users);

    public void deleteCategory(Long id);

}