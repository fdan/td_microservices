package io.testio.Article.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import io.testio.Article.models.Comment;

@Repository
public interface CommentRepository extends JpaRepository<Comment, Long> {
}