package io.testio.Article.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import lombok.Data;

@Data
@Entity
public class Comment {

    @Id
    @GeneratedValue
    private Long id;
    private String title;
    private String body;
    // private int article;
    // private int user;
}