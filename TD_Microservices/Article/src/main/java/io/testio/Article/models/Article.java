package io.testio.Article.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import lombok.Data;

@Data
@Entity
public class Article {

    @Id
    @GeneratedValue
    private Long id;
    private String title;
}