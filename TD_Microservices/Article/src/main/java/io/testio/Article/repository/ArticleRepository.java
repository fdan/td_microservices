package io.testio.Article.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import io.testio.Article.models.Article;

@Repository
public interface ArticleRepository extends JpaRepository<Article, Long> {
}