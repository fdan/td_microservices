package io.testio.Utilisateur.Services;

import java.util.List;
import io.testio.Utilisateur.models.UsersEntity;

public interface UserService {

    public List<UsersEntity> getALLUsers();

    public UsersEntity getUser(Long id);

    public void saveUser(UsersEntity users);

    public void deleteUser(Long id);

    public void updateUser(UsersEntity users);

}