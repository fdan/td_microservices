package io.testio.Utilisateur.models;

import javax.persistence.*;
import lombok.*;

@Data
@Entity
public class UsersEntity {

    @Id
    @GeneratedValue
    private Long id;
    private String firstName;
    private String lastName;
    private String email;
    private String password;
}