package io.testio.Utilisateur.Controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

import io.testio.Utilisateur.Services.UserService;
import io.testio.Utilisateur.models.UsersEntity;

@RestController
public class UserController {

    @Autowired
    private UserService service;

    @GetMapping(value = "/users")
    public ResponseEntity<List<UsersEntity>> getAllUsers() {
        List<UsersEntity> users = service.getALLUsers();
        return new ResponseEntity<>(users, HttpStatus.OK);
    }

    @GetMapping(value = "/users/{id}")
    public ResponseEntity<UsersEntity> getUser(@PathVariable Long id) {
        UsersEntity user = service.getUser(id);
        return new ResponseEntity<>(user, HttpStatus.OK);
    }

    @PostMapping(value = "/users")
    public ResponseEntity<Void> addUser(@RequestBody UsersEntity users) {
        service.saveUser(users);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PutMapping(value = "/users/{id}")
    public ResponseEntity<Void> updateUser(@RequestBody UsersEntity user, @PathVariable Long id) {
        UsersEntity users = service.getUser(id);
        if (users != null) {
            users.setFirstName(user.getFirstName());
            users.setLastName(user.getLastName());
            service.updateUser(users);
        }
        return new ResponseEntity<Void>(HttpStatus.OK);
    }

    @DeleteMapping(value = "/users/{id}")
    public ResponseEntity<Void> deleteUser(@PathVariable Long id) {
        service.deleteUser(id);
        return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
    }
}