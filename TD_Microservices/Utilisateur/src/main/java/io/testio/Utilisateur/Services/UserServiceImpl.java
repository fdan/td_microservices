package io.testio.Utilisateur.Services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import io.testio.Utilisateur.Repository.UsersRepository;
import io.testio.Utilisateur.models.UsersEntity;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UsersRepository repository;

    @Override
    public List<UsersEntity> getALLUsers() {
        List<UsersEntity> employees = (List<UsersEntity>) repository.findAll();
        return employees;
    }

    @Override
    public UsersEntity getUser(Long id) {
        Optional<UsersEntity> optEmp = repository.findById(id);
        return optEmp.get();
    }

    @Override
    public void saveUser(UsersEntity users) {
        repository.save(users);
    }

    @Override
    public void deleteUser(Long id) {
        repository.deleteById(id);
    }

    @Override
    public void updateUser(UsersEntity users) {
        repository.save(users);
    }
}
