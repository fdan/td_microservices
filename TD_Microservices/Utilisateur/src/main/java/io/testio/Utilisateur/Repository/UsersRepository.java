package io.testio.Utilisateur.Repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import io.testio.Utilisateur.models.UsersEntity;

@Repository
public interface UsersRepository extends CrudRepository<UsersEntity, Long> {
}